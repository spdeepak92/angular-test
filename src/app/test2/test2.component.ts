import { Component, OnInit } from '@angular/core';
import { ShareService } from '../share.service';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrls: ['./test2.component.css']
})
export class Test2Component implements OnInit {

  public msg;

  constructor(public _share: ShareService) { }

  ngOnInit() {
  }

  updatemsg() {
    this._share.updateMsg(this.msg);
  }

}
