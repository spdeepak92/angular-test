import { Component } from '@angular/core';
import { ShareService } from './share.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public message;

  constructor(public _share: ShareService) {
    this._share.emitValue.subscribe(
      data => this.message = data
    );
  }

}
