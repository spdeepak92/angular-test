import { Component, OnInit } from '@angular/core';
import { ShareService } from '../share.service';

@Component({
  selector: 'app-test3',
  templateUrl: './test3.component.html',
  styleUrls: ['./test3.component.css']
})
export class Test3Component implements OnInit {

  public msg;

  constructor(public _share: ShareService) { }

  ngOnInit() {
  }

  updatemsg() {
    this._share.updateMsg(this.msg);
  }

}
