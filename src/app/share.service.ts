import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShareService {

  public emitValue = new BehaviorSubject('Default Message');

  private broadcastValue = this.emitValue.asObservable();

  constructor() { }

  updateMsg(msg) {
    this.emitValue.next(msg);
  }

  complete() {
    this.emitValue.next('Default Message');
    this.emitValue.complete();
  }
}
