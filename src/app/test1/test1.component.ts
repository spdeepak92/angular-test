import { Component, OnInit } from '@angular/core';
import { ShareService } from '../share.service';

@Component({
  selector: 'app-test1',
  templateUrl: './test1.component.html',
  styleUrls: ['./test1.component.css']
})
export class Test1Component implements OnInit {

  public msg;

  constructor(public _share: ShareService) { }

  ngOnInit() {
  }

  updatemsg() {
    this._share.updateMsg(this.msg);
  }
}
