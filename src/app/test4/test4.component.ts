import { Component, OnInit } from '@angular/core';
import { ShareService } from '../share.service';

@Component({
  selector: 'app-test4',
  templateUrl: './test4.component.html',
  styleUrls: ['./test4.component.css']
})
export class Test4Component implements OnInit {

  public msg;

  constructor(public _share: ShareService) { }

  ngOnInit() {
  }

  updatemsg() {
    this._share.updateMsg(this.msg);
  }
}
