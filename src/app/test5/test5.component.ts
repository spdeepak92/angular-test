import { Component, OnInit } from '@angular/core';
import { ShareService } from '../share.service';

@Component({
  selector: 'app-test5',
  templateUrl: './test5.component.html',
  styleUrls: ['./test5.component.css']
})
export class Test5Component implements OnInit {

  constructor(public _share: ShareService) { }

  ngOnInit() {
  }

  complete() {
    this._share.complete();
  }

}
